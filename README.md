# Установка

Для того чтобы развернуть приложение в dev окружении с рабочими volume
необходимо запустить следующие команды:

`chmod u+x build_dev.sh`

Затем нужно запустить:

`./build_dev.sh ${app_name} ${host}`

app_name: имя проекта (параметр необходим для соединения с postgresql, а также это имя будет использовано в nginx для проксирования через fastcgi_pass)

host: хост и порт до проекта
